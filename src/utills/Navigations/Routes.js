import * as React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import SignupScreen from '../../Screen/SignupScreen';
const Stack = createNativeStackNavigator();

export const authStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="SignupScreen" component={SignupScreen} />
    </Stack.Navigator>
  );
};
